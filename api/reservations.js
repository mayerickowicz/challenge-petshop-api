import { CREATED, NO_CONTENT } from './status.js'
import { formatCollection } from './collections.js'
import { ObjectNotFound } from 'djorm/errors.js'
import { Reservation } from '../db/models.js'

export const reservationObject = fastify => {
  fastify.get(
    '/',
    async req => await Reservation.objects.get({ id: req.params.reservationId })
  )
  fastify.delete('/', async (req, res) => {
    try {
      const reservation = await Reservation.objects.get({
        id: req.params.reservationId,
      })
      await reservation.delete()
    } catch (e) {
      if (!(e instanceof ObjectNotFound)) {
        throw e
      }
    }
    res.status(NO_CONTENT)
  })

  return Promise.resolve()
}

export const reservationCollection = fastify => {
  fastify.register(reservationObject, { prefix: '/:reservationId' })
  fastify.addSchema({
    $id: 'models/Reservation',
    type: 'object',
    additionalProperties: false,
    required: ['ownerId', 'petId'],
    properties: {
      ownerId: {
        type: 'integer',
      },
      petId: {
        type: 'integer',
      },
      since: {
        type: 'string',
        format: 'date-time',
      },
      until: {
        type: 'string',
        format: 'date-time',
      },
    },
  })

  fastify.get('/', async () =>
    formatCollection(await Reservation.objects.all())
  )

  // add POST endpoint
  fastify.route({
    method: 'POST',
    url: '/',
    schema: {
      body: { $ref: 'models/Reservation' },
    },
    handler: async (req, res) => {
      const reservation = await Reservation.create(req.body)
      res.status(CREATED)
      return reservation
    },
  })

  return Promise.resolve()
}
