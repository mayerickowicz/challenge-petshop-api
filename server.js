import createFastify from 'fastify'
import logger from 'djorm/logger.js'

import { getSettings, init } from 'djorm/config.js'
import { reservationCollection } from './api/reservations.js'
import { personCollection } from './api/people.js'
import { petCollection } from './api/pets.js'
import { root } from './api/root.js'
import { FieldValidationError } from 'djorm/errors.js'

import './config.js'

const PORT_DEFAULT = 3000
const EXIT_CODE_CRASH = 255
const INTERNAL_SERVER_ERROR = 500
const BAD_REQUEST = 400
const port = process.env.NODE_PORT || PORT_DEFAULT

export const createServer = () => {
  const fastify = createFastify({
    logger: logger.getLogger(),
    trustProxy: true,
  })
  fastify.addContentTypeParser(
    'application/json',
    { parseAs: 'string' },
    (req, body, done) => {
      try {
        done(null, JSON.parse(body))
      } catch (err) {
        err.statusCode = BAD_REQUEST
        done(err)
      }
    }
  )

  fastify.register(reservationCollection, { prefix: '/reservations' })
  fastify.register(petCollection, { prefix: '/pets' })
  fastify.register(personCollection, { prefix: '/people' })
  fastify.register(root)
  fastify.setErrorHandler((error, req, res) => {
    if (error instanceof FieldValidationError) {
      res.status(BAD_REQUEST).send({
        fields: [
          {
            code: error.code,
            field: error.propertyName,
            message: error.message,
            value: error.value,
          },
        ],
      })
    } else {
      res.status(INTERNAL_SERVER_ERROR).send({
        code: 'internal-server-error',
        message: error.message,
      })
    }
  })
  return fastify
}

export const start = async () => {
  try {
    await init()
    const server = createServer()
    await server.listen(getSettings('apiPort', port))
    return server
  } catch (err) {
    logger.error(err)
    process.exit(EXIT_CODE_CRASH)
  }
  return null
}
